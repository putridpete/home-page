# Home Page

This is a very simple static HTML page designed to serve as an alternative to overtly complicated Dashboards, mostly for my personal use. It functions as both my home page and as an easy way to access my self-hosted services.

## Screenshots
![Screenshot](https://peterdominguez.art/wp-content/uploads/2023/08/home-page.png "This is a screenshot.")

## Installation

Simply clone this repository in your home directory and rename it to something like `/.homepage/` and run `start.html`. Don't forget to substitute any instance of `/home/peter/` in `start.html` for your username instead; also, set your own time zone by modifying the line that says `date = new Date().toLocaleString("es-DO", {timeZone: "America/Santo_Domingo"})` for your own location.

 If you intend to use this in a local or remote server, consider renaming it to `/homepage/` and place it somewhere where your web server of choice can read it; keep in mind that I've only tested this with apache, so I can't really say how it will work for anything else.

Make sure to give it the apropriate permissions:

* `sudo chown -R www-data:www-data /location/of/your/homepage`

* `sudo chmod -R 755 /location/of/your/homepage`

## Configuration

Home Page was designed with Firefox use in mind, and coupled with the following tools:

* [arkenfox-user.js](https://github.com/arkenfox/user.js): Firefox user.js template for configuration and hardening.
A web extension that redirects YouTube, Twitter, TikTok, and other websites to alternative privacy friendly frontends
* [libredirect](https://libredirect.github.io/): A web extension that redirects several websites to alternative privacy friendly frontends.
* [FF-Container-HomePages](https://github.com/MoAllabbad/FF-Container-HomePages): Firefox browser extension that lets you specify a default page for each container (although you could use other similar tools if you prefer).
* A web server; [apache](https://www.apache.org/dist/httpd) recommended.

The idea is to host it either locally or remotely so it can be used with the `FF-Container-HomePages` addon, because local files are no longer supported as the homepage for new tabs in Firefox due to security reasons. Home Page can be used as a regular Firefox homepage, however; you can access it at any time by using the keybind Alt+Home key.

Other than that, the `arkenfox-user.js` script and the addon `libredirect` are paired together for better privacy. Because the script in question enables letterboxing, the styling of the page was designed with that in mind, and using it without it may lead to certain consistency issues in how the elements are laid out; also, the main search bar uses the aforementioned addon by default to search using various instances of SearXNG (provided you have it installed and configured to do so before hand), but this can be changed by modifying `start.html`.

## Support
Since this is a personal project, don't expect much support; this is provided as is. That said, I'll do what I can to assist you if it's within reason.

## Common issues

Some common issues you may encounter are:

* Images not displaying properly: this issue will likely occur when failing to adjust `start.html` for the new paths that you will be using. Because this is a personal project, and something that I will need to clone in other computers, I've left my desired paths intact; you should change them to the ones you need.
* But they still don't show: If images are not coming up even when the paths are correct, then you're likely facing a permissions issue. Please refer to the Installation section and run the required commands listed there.
* The search bar is not working: As I mentioned in the Configuration step, you need to install and configure the `libredirect` addon to make it work with SearXNG or, alternatively, edit `start.html` to add the search engine of your choice.
* When I click on qbittorrent-nox from Home Page, I get an error message: This can be bypassed by going into the settings page of qbittorrent-nox, then the webui section, and disabling CSRF. **Warning: this is not advisable if you host it remotely.**   

## Contributing
Other than fixes to my shitty code, I won't change this drastically, so don't open up a MR asking me to overhaul it in ways that are out of the scope of being a simple personal dashboard.

## Authors and acknowledgment
I pieced this together mostly from code I lifted or modified from [w3schools](https://www.w3schools.com) and a few other sites I can't remember and is only intended for personal use, please don't sue me. If you find your code here and want me to credit you, please open up a merge request and I will be happy to.

Images and logos are owned by their respective copyrightholders and are in no way affiliated with this small side project.

## License
Whatever code is original, is licensed under the GNU GPLv3. Any borrowed or modified code is subject to its original license(s) where applicable.
